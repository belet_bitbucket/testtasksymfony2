<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160708121246 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tblProductData ADD dtmTimestamp DATETIME NOT NULL, ADD intStock INT UNSIGNED NOT NULL, ADD floatPrice DOUBLE PRECISION NOT NULL, DROP stmTimestamp');
        $this->addSql('DROP INDEX strproductcode ON tblProductData');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2C11248662F10A58 ON tblProductData (strProductCode)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tblProductData ADD stmTimestamp DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, DROP dtmTimestamp, DROP intStock, DROP floatPrice');
        $this->addSql('DROP INDEX uniq_2c11248662f10a58 ON tblProductData');
        $this->addSql('CREATE UNIQUE INDEX strProductCode ON tblProductData (strProductCode)');
    }
}
