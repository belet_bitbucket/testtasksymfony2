<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 08.07.2016
 * Time: 12:15
 */

namespace Tests\AppBundle\Classes\Importer;

use AppBundle\Entity\ProductData;
use AppBundle\Classes\Exporter\CsvFileExporter;
use AppBundle\Classes\ImportHelper\ImportDataHelper;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator\RecursiveValidator;

class ImportDataHelperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test ImportDataHelperOnTestFile
     */
    public function testImportDataHelperOnTestFile()
    {
        $filename = 'testFile.csv';
        $csvFileExporter = new CsvFileExporter($filename);
        $entityManagerMock = $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entityValidatorMock = $this
            ->getMockBuilder(RecursiveValidator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $importer = new ImportDataHelper($entityManagerMock, $entityValidatorMock, $csvFileExporter, false);
        $entityManagerMock
            ->expects($this->once())
            ->method('persist')
            ->with($this->isInstanceOf(ProductData::class));
        $entityManagerMock->expects($this->once())->method('flush');
        $importer->import();
        $this->assertEquals(3, count($importer->getErrors()), "Error counter invalid");
    }
}
