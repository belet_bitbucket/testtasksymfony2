<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 06.07.2016
 * Time: 11:45
 */

namespace Tests\AppBundle\Classes\Exporter;

use AppBundle\Classes\Exporter\StreamCsvFileExporter;

class StreamCsvFileExporterTest extends CsvFileExporterTest
{
    /**
     * call before each test
     */
    public function setUp()
    {
        $this->exporterName = StreamCsvFileExporter::class;
    }

    /**
     * Test StreamCsvFileExporter on big file.
     */
    public function testStreamExport()
    {
        $filename = 'bigTestFile.csv';
        $exporter = new $this->exporterName($filename);
        $validRecords = $exporter->getPartOfValidData(3, true);
        $allValidRecords = [];
        $allValidRecords = array_merge($allValidRecords, $validRecords);
        $errors = $exporter->getErrors();
        $this->assertEquals(array(
            ['P0001','TV','32” Tv','10','399.99',''],
            ['P0002','TV','32” Tv','10','399.99',''],
            ['P0003','TV','32” Tv','10','399.99','']
        ), $validRecords);
        $this->assertEquals(array(
            'Error in line 2: Product code format is incorrect',
            'Error in line 3: Price greater than $1000',
            'Error in line 4: Field Stock is unset',
            'Error in line 6: Product code format is incorrect',
            'Error in line 7: Price greater than $1000',
            'Error in line 8: Field Stock is unset'
        ), $errors);
        while (count($validRecords) !== 0) {
            unset($validRecords);
            $validRecords = $exporter->getPartOfValidData(3, false);
            $allValidRecords = array_merge($allValidRecords, $validRecords);
            $errors = array_merge($errors, $exporter->getErrors());
        }
        $this->assertEquals(13, count($allValidRecords));
        $this->assertEquals(39, count($errors));
    }
}
