<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 06.07.2016
 * Time: 11:45
 */

namespace Tests\AppBundle\Classes\Exporter;

use AppBundle\Classes\Exporter\CsvFileExporter;

class CsvFileExporterTest extends \PHPUnit_Framework_TestCase
{
    /** @var  string */
    protected $exporterName;
    
    /** call before each test */
    public function setUp()
    {
        $this->exporterName = CsvFileExporter::class;
    }

    /**
     * Test FileExporter ability to check whether a file exists.
     */
    public function testExporterFileChecker()
    {
        $validFileNames = ['stock.csv'];
        $invalidFileNames = [
            'D:\work\test\sock.csv',
            'sock.csv',
            'D:/work/test/sock.csv'
        ];
        foreach ($validFileNames as $num => $validFileName) {
            $csvFileExporter = new $this->exporterName($validFileName);
            $csvFileExporter->getValidData();
            $errors = $csvFileExporter->getErrors();
            $this->assertTrue(
                !in_array('Error: File doesn\'t exist.', $errors),
                sprintf('File \'$s exist but defined as non existed', $validFileName)
            );
            unset($error);
        }
        foreach ($invalidFileNames as $invalidFileName) {
            $csvFileExporter = new $this->exporterName($invalidFileName);
            $csvFileExporter->getValidData();
            $errors = $csvFileExporter->getErrors();
            $this->assertTrue(
                in_array('Error: File doesn\'t exist.', $errors),
                sprintf('File \'$s doesn\'t exist but defined as existed', $invalidFileName)
            );
            unset($error);
        }
    }

    /**
     * Test FileExporter on testFile with 3 records.
     */
    public function testCsvFileExporterOnTestFile()
    {
        $filename = 'testFile.csv';
        $csvFileExporter = new $this->exporterName($filename);
        $validRecords = $csvFileExporter->getValidData();
        $errors = $csvFileExporter->getErrors();
        $this->assertTrue(count($validRecords)===1);
        $this->assertTrue(count($errors)===3);
        $this->assertTrue(in_array(['P0001','TV','32” Tv','10','399.99',''], $validRecords));
        $this->assertEquals(array(
            'Error in line 2: Product code format is incorrect',
            'Error in line 3: Price greater than $1000',
            'Error in line 4: Field Stock is unset'
        ), $errors);
    }
}
