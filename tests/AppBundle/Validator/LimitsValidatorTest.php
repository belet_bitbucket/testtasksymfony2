<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 06.07.2016
 * Time: 11:12
 */

namespace Tests\AppBundle\Validator;

class LimitsValidatorTest extends ValidatorTestBase
{
    /**
     * InputRecordValidator class test.
     * Records where price > 1000 and where stock < 10 && price > 5 must be identified as invalid.
     */
    public function testValidatorWithErrorInProductCode()
    {
        $this->invalidRecords = array(
            ['P0001', '24” Monitor', 'Best.console.ever', '5', '4', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '8', '1001', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '9', '4', '']
        );
        $this->validRecords = array(
            ['P0001', '24” Monitor', 'Best.console.ever', '11', '4', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '4', '1000', '']
        );
        $this->runTests();
    }
}
