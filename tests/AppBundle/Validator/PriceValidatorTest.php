<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 04.07.2016
 * Time: 17:30
 */

namespace Tests\AppBundle\Validator;

class PriceValidatorTest extends ValidatorTestBase
{
    /**
     * InputRecordValidator class test.
     * Price must be float.
     */
    public function testValidatorWithErrorInPrice()
    {
        $this->invalidRecords = array(
            ['P0001', '24” Monitor', 'Best.console.ever', '5', '-30.44', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '5', '$30.44', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '5', '30.4.4', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '5', 'Igor', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '5', '', '']
        );
        $this->validRecords = array(
            ['P0001', '24” Monitor', 'Best.console.ever', '5', '30.44', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '5', '10', '']
        );
        $this->runTests();
    }
}
