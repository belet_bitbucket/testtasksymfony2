<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 04.07.2016
 * Time: 16:27
 */

namespace Tests\AppBundle\Validator;

class ProductCodeValidatorTest extends ValidatorTestBase
{
    /**
     * InputRecordValidator class test.
     * Product code has format PXXXX where X - digit.
     */
    public function testValidatorWithErrorInProductCode()
    {
        $this->invalidRecords = array(
            ['P00011', '24” Monitor', 'Best.console.ever', '5', '30.44', ''],
            ['A0001', '24” Monitor', 'Best.console.ever', '5', '30.44', ''],
            ['0001', '24” Monitor', 'Best.console.ever', '5', '30.44', ''],
            ['0001P', '24” Monitor', 'Best.console.ever', '5', '30.44', ''],
            ['PA00', '24” Monitor', 'Best.console.ever', '5', '30.44', ''],
        );
        $this->validRecords = array(['P0001', '24” Monitor', 'Best.console.ever', '5', '30.44', '']);
        $this->runTests();
    }
}
