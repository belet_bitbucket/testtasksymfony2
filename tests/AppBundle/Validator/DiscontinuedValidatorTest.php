<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 04.07.2016
 * Time: 17:33
 */

namespace Tests\AppBundle\Validator;

class DiscontinuedValidatorTest extends ValidatorTestBase
{
    /**
     * InputRecordValidator class test.
     * Validator of discontinued parameter value test.
     */
    public function testRecordValidatorDiscontinued()
    {
        $this->validRecords = array(
            ['P0001', '24” Monitor', 'Best.console.ever', '4', '30.44', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '4', '30.44', 'yes'],
            );
        $this->invalidRecords = array(
            ['P0001', '24” Monitor', 'Best.console.ever', '4', '30.44', 'no'],
            ['P0001', '24” Monitor', 'Best.console.ever', '4', '30.44', 'true']);
        $this->runTests();
    }
}
