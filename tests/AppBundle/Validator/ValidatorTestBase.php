<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 06.07.2016
 * Time: 10:53
 */

namespace Tests\AppBundle\Validator;

use AppBundle\Classes\Exporter\Validator\InputRecordValidator;

class ValidatorTestBase extends \PHPUnit_Framework_TestCase
{
    /** @var array  */
    protected $validRecords = [];

    /** @var array  */
    protected $invalidRecords = [];

    /**
     * Execute InputRecordValidator tests.
     */
    protected function runTests()
    {
        $validator = new InputRecordValidator();
        foreach ($this->validRecords as $num => $validRecord) {
            $this->assertTrue(
                $validator->isRecordValid($validRecord),
                sprintf('valid record number %d has error: %s', $num, $validator->getError())
            );
        }
        foreach ($this->invalidRecords as $num => $invalidRecord) {
            $this->assertNotTrue(
                $validator->isRecordValid($invalidRecord),
                sprintf('invalid record number %d defined as a correct', $num)
            );
        }
    }
}
