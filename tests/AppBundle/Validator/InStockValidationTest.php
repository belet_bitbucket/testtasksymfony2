<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 04.07.2016
 * Time: 17:26
 */

namespace Tests\AppBundle\Validator;

class InStockValidationTest extends ValidatorTestBase
{
    /**
     * InputRecordValidator class test.
     * Validator of stock parameter value test.
     */
    public function testValidatorWithErrorInStock()
    {
        $this->invalidRecords = array(
            ['P0001', '24” Monitor', 'Best.console.ever', 'A', '30.44', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '-5', '30.44', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '30.44', '30.44', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '', '30.44', '']
        );
        $this->validRecords = array(
            ['P0001', '24” Monitor', 'Best.console.ever', '0', '30.44', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '1000', '30.44', ''],
            ['P0001', '24” Monitor', 'Best.console.ever', '5', '30.44', '']
        );
        $this->runTests();
    }
}
