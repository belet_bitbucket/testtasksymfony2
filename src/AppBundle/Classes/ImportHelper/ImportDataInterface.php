<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 15.07.2016
 * Time: 12:51
 */

namespace AppBundle\Classes\ImportHelper;


interface ImportDataInterface
{
    /**
     * Import data function
     * @abstract
     * @param $data
     *
     * @return bool
     */
    public function import($data);

    /**
     * Returns array with all errors.
     * @abstract
     *
     * @return array
     */
    public function getErrors();
}