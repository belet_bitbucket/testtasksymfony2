<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 05.07.2016
 * Time: 17:09
 */

namespace AppBundle\Classes\ImportHelper;

use AppBundle\Entity\ProductData;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ImportDataHelper implements ImportDataInterface
{
    
    /** @var \Doctrine\ORM\EntityManager  */
    protected $entityManager;
    
    /** @var ValidatorInterface  */
    protected $entityValidator;
    
    /** @var array  */
    protected $errors = [];

    /**
     * ImportDataHelper constructor.
     * @param EntityManager $entityManager
     * @param ValidatorInterface $entityValidator
     */
    public function __construct(
        EntityManager $entityManager,
        ValidatorInterface $entityValidator
    ) {
        $this->entityManager = $entityManager;
        $this->entityValidator = $entityValidator;
    }
    
    /**
     * Return array with errors appeared during import.
     * Empty until import called.
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Execute import to database operation.
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function import($data)
    {
        unset($this->errors);
        $this->errors = [];
        try {
            foreach ($data as $record) {
                $this->pushRecordToEm($record);
            }
            $this->entityManager->flush();
        } catch (\Exception $e) {
            throw $e;
        }
        return true;
    }

    /**
     * Push valid records to entity manager
     * @param array $record
     */
    protected function pushRecordToEm($record)
    {
        $productData = new ProductData(
            $record[1],
            $record[2],
            $record[0],
            intval($record[3]),
            floatval($record[4])
        );
        if ($record[5] === 'yes') {
            $productData->setDtmDiscontinued(new \DateTime());
        }
        
        $errors = $this->entityValidator->validate($productData);
        if (count($errors) !== 0) {
            foreach ($errors as $error) {
                $this->errors[] = sprintf(
                    'Product with product code - %s will not be imported. %s',
                    $record[0],
                    $error->getMessage()
                );
            }
        } else {
            $this->entityManager->persist($productData);
        }
    }
}
