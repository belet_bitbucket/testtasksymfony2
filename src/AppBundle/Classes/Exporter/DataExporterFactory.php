<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 07.07.2016
 * Time: 16:34
 */

namespace AppBundle\Classes\Exporter;

class DataExporterFactory
{
    /**
     * Return chosen exporter class.
     * @param string $exporterName
     * @param mixed $source
     * @param ErrorHandlerInterface|null $handler
     * @return DataExporterInterface
     */
    public static function create($exporterName, $source, ErrorHandlerInterface $handler = null)
    {
        return new $exporterName($source,$handler);
    }
}
