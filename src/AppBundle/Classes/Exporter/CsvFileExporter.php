<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 05.07.2016
 * Time: 19:39
 */

namespace AppBundle\Classes\Exporter;

class CsvFileExporter implements DataExporterInterface
{

    /** @var string  */
    protected $filename = '';

    /** @var  string */
    private $fileMd5Hash;

    /** @var string */
    private $autoDetectLineEndingsValue;
    
    /** @var resource  */
    private $fileHandle;


    /**
     * CsvFileExporter constructor.
     * @param $filename
     * @throws \RuntimeException if file does'n exist or can't be opened.
     */
    public function __construct($filename)
    {
        $this->autoDetectLineEndingsValue = ini_get('auto_detect_line_endings');
        ini_set('auto_detect_line_endings', '1');
        $this->filename = $filename;
        if (!$this->checkIsFileExist()) {
            throw new \RuntimeException(sprintf('File with filename \'%s\' doesn\'t exist', $this->filename));
        }
        $this->calcFileMd5Hash();
        if (($this->fileHandle = fopen($filename, 'r')) === false) {
            throw new \RuntimeException(sprintf('File can\'t de opened for reading'));
        }
    }

    /**
     * CsvFileExporter destructor.
     * Set auto_detect_line_endings to default value.
     */
    public function __destruct()
    {
        ini_set('auto_detect_line_endings', $this->autoDetectLineEndingsValue);
        fclose($this->fileHandle);
    }

    /**
     * Calculate current data file md5hash and save it into the property $fileMd5Hash.
     */
    protected function calcFileMd5Hash()
    {
        $this->fileMd5Hash = md5_file($this->filename);
    }

    /**
     * Check is current data file md5hash equals to md5hash stored in property $fileMd5Hash.
     * @return bool
     */
    protected function checkFileMd5Hash()
    {
        return $this->fileMd5Hash === md5_file($this->filename);
    }

    /**
     * Check is file with filename $this->filename exist.
     * @return bool
     */
    protected function checkIsFileExist()
    {
        return is_file($this->filename);
    }

    /**
     * Return array with Csv file data.
     * @param int $arrayMaxLength
     * @return array
     * @throws \Exception witch appears during method work.
     */
    public function getData($arrayMaxLength)
    {
        try {
            $this->prepareForDataExport($arrayMaxLength);
            $result = array();
            while (count($result) < $arrayMaxLength) {
                $row = fgetcsv($this->fileHandle, 1000, ",");
                if ($row !== false) {
                    $result[] = $row;
                } else {
                    break;
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
        return $result;
    }

    /**
     * Check params format and values.
     * @param $arrayMaxLength
     * @throws \RuntimeException if file was deleted or changed.
     * @throws \UnexpectedValueException if $arrayMaxLength not greater than 0.
     */
    private function prepareForDataExport($arrayMaxLength)
    {
        unset($this->errors);
        $this->errors = [];
        if (!$this->checkIsFileExist() || !$this->checkFileMd5Hash()) {
            throw new \RuntimeException('Error: File was deleted or changed.');
        }
        if ($arrayMaxLength <= 0) {
            throw new  \UnexpectedValueException('Error calling getValidData. Array max length must be greater than 0');
        }
    }
}
