<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 06.07.2016
 * Time: 11:18
 */

namespace AppBundle\Classes\Exporter;

interface DataExporterInterface
{
    /**
     * Return valid data array
     * @abstract
     *
     * @param int $arrayMaxLength
     * @return array
     */
    public function getData($arrayMaxLength);
    
}
