<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 04.07.2016
 * Time: 14:48
 */

namespace AppBundle\Classes\Validator;

use Exception;

class InputRecordValidator
{
    /** @var string  */
    private $error = '';

    /** @var array */
    private $fieldNames = [
        'Product Code',
        'Product Name',
        'Product Description',
        'Stock',
        'Cost in GBP',
        'Discontinued'
    ];

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Check is input row valid. If not push error message to input $error array.
     * @param array $row
     *
     * @return bool
     */
    public function isRecordValid($row)
    {
        try {
            $this->checkIsEachFieldSet($row);
            $this->checkProductCode($row[0]);
            $this->checkStockValue($row[3]);
            $this->checkPriceValue($row[4]);
            $this->checkDiscontinuedValue($row[5]);
            $this->checkLimits($row[4], $row[3]);
        } catch (Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Check is product code valid. If not throw exception.
     * @param string $value
     * @throws Exception
     */
    private function checkProductCode($value)
    {
        if (preg_match('#P\\d\\d\\d\\d#', $value) === 0 || strlen($value) !== 5) {
            throw new Exception('Product code format is incorrect');
        }
    }

    /**
     * Check is stock value valid. If not throw exception.
     * @param string $value
     * @throws Exception
     */
    private function checkStockValue($value)
    {
        if (!ctype_digit($value)) {
            throw new Exception('In stock should be positive integer');
        }
    }

    /**
     * Check is price valid. If not throw exception.
     * @param string $value
     * @throws Exception
     */
    private function checkPriceValue($value)
    {
        if (!is_numeric($value)) {
            throw new Exception('Price should be numeric type');
        }
    }

    /**
     * Check is Discontinued param value valid. If not throw exception.
     * @param string $value
     * @throws Exception
     */
    private function checkDiscontinuedValue($value)
    {
        if ($value!=='yes' && $value!=='') {
            throw new Exception('Discontinued may have only values "yes" and empty value');
        }
    }

    /**
     * Check limits on price and stock values. If values invalid throw exception.
     * @param $price
     * @param $stock
     * @throws Exception
     */
    private function checkLimits($price, $stock)
    {
        if ($price>1000) {
            throw new Exception('Price greater than $1000');
        } if ($price < 5 && $stock < 10) {
            throw new Exception('Price less than $5 and stock less than 10');
        }
    }

    /**
     * Check is each field in row has value
     * @param $row
     * @throws Exception
     */
    private function checkIsEachFieldSet($row)
    {
        foreach ($this->fieldNames as $index => $fieldName) {
            if (!isset($row[$index])) {
                throw new Exception(sprintf('Field %s is unset', $fieldName));
            }
        }
    }
}
