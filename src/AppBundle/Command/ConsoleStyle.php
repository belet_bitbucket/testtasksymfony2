<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 12.07.2016
 * Time: 17:13
 */

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConsoleStyle extends SymfonyStyle
{
    /**
     * ConsoleStyle constructor.
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function __construct(InputInterface $input, OutputInterface $output)
    {
        parent::__construct($input, $output);
    }
    
    /**
     * Warning handling method.
     * @param string $message
     */
    public function warning($message)
    {
        $this->writeln(sprintf('<fg=red>[WARNING]%s', $message));
    }
    
    /**
     * Note handling method.
     * @param string $message
     */
    public function note($message)
    {
        $this->writeln('======================');
        $this->writeln($message);
        $this->writeln('======================');
    }
}
