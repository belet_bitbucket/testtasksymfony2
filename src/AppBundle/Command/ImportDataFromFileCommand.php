<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 04.07.2016
 * Time: 10:39
 */

namespace AppBundle\Command;

use AppBundle\Classes\Exporter\CsvFileExporter;
use AppBundle\Classes\Exporter\DataExporterInterface;
use AppBundle\Classes\ImportHelper\ImportDataInterface;
use AppBundle\Classes\Validator\InputRecordValidator;
use Doctrine\DBAL\Exception\ConnectionException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportDataFromFileCommand extends ContainerAwareCommand
{
    /**
     * ImportDataFromFileCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Configure new console command
     */
    protected function configure()
    {
        $this
            ->setName('appBundle:database:import')
            ->setDescription('Import data from csv file to database.')
            ->addArgument(
                'filename',
                InputOption::VALUE_REQUIRED,
                'Path and name of your csv file.'
            )
            ->addOption(
                'test',
                't',
                InputOption::VALUE_OPTIONAL,
                'Execute operation without database update.'
            )
            ->addOption(
                'recordsPerOneIteration',
                'r',
                InputOption::VALUE_OPTIONAL,
                'Execute operation without database update.',
                5
            );

    }

    /**
     * Execute console command operation.
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $styleOutput = new ConsoleStyle($input, $output);
        $filename = $input->getArgument('filename');
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        try {
            $styleOutput->title('Import data from csv file to database');

            $em->getConnection()->beginTransaction();
            $exporter = $this->getContainer()
                ->get('app.data_exporter.factory')
                ->create(CsvFileExporter::class, $filename);
            $importer = $this->getContainer()->get('app.import_helper');

            $recordsPerOneIteration = intval($input->getOption('recordsPerOneIteration'));
            $this->executeImport($exporter, $importer, $styleOutput, $recordsPerOneIteration);

            if ($input->getOption('test')) {
                $em->getConnection()->rollback();
            } else {
                $em->getConnection()->commit();
            }
            $styleOutput->success('Import finished.');
        } catch (ConnectionException $e) {
            $styleOutput->error($e->getMessage());
        } catch (\Exception $e) {
            $styleOutput->error(sprintf('Critical error: %s. Rollback all changes.', $e->getMessage()));
            $em->getConnection()->rollback();
        }
    }

    /**
     * Execute console import operation.
     * @param DataExporterInterface $exporter
     * @param ImportDataInterface $importer
     * @param ConsoleStyle $output
     * @param int $recordsPerOneIteration
     * @throws \Exception
     */
    private function executeImport(
        DataExporterInterface $exporter,
        ImportDataInterface $importer,
        ConsoleStyle $output,
        $recordsPerOneIteration
    ) {
        try {
            $currentRowNum = 1;
            $validator = new InputRecordValidator();
            do {
                $records = $exporter->getData($recordsPerOneIteration);
                foreach ($records as $index => $record) {
                    if (!$validator->isRecordValid($record)) {
                        unset($records[$index]);
                        $output->warning(sprintf('Error in record %d: %s', $currentRowNum, $validator->getError()));
                    }
                    $currentRowNum++;
                }

                $importer->import($records);
                foreach ($importer->getErrors() as $error) {
                    $output->warning($error);
                }
            } while (count($records) !== 0);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
