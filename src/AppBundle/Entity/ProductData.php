<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductData
 *
 * @ORM\Table(name="tblProductData")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductDataRepository")
 * @UniqueEntity(fields={"strProductCode"},message="Product code must be unique.")
 * @ORM\HasLifecycleCallbacks()
 */
class ProductData
{
    /**
     * @var int
     *
     * @ORM\Column(name="intProductDataId", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Type(type="string",message="Product name must has string type")
     * @ORM\Column(name="strProductName", type="string", length=50)
     */
    private $strProductName;

    /**
     * @var string
     *
     * @Assert\Type(type="string",message="Product name must have string type")
     * @ORM\Column(name="strProductDesc", type="string", length=255)
     */
    private $strProductDesc;

    /**
     * @var string
     *
     * @Assert\Type(type="string",message="Product name must have string type")
     * @ORM\Column(name="strProductCode", type="string", length=10, unique=true)
     */
    private $strProductCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtmAdded", type="datetime", nullable=true)
     */
    private $dtmAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtmDiscontinued", type="datetime", nullable=true)
     */
    private $dtmDiscontinued;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtmTimestamp", type="datetime")
     */
    private $dtmTimestamp;

    /**
     * @var int
     *
     * @Assert\Type(type="int",message="Stock must have integer type")
     * @ORM\Column(name="intStock", type="integer", options={"unsigned"=true})
     */
    private $intStock;

    /**
     * @var float
     *
     * @Assert\Type(type="float",message="Stock must have float type")
     * @ORM\Column(name="floatPrice", type="float", options={"unsigned"=true})
     */
    private $floatPrice;


    public function __construct(
        $strProductName,
        $strProductDesc,
        $strProductCode,
        $intStock,
        $floatPrice
    ) {
        $this->strProductName = $strProductName;
        $this->strProductDesc = $strProductDesc;
        $this->strProductCode = $strProductCode;
        $this->intStock = $intStock;
        $this->floatPrice = $floatPrice;
        $this->dtmAdded = new \DateTime();
    }

    /**
     * @return float
     */
    public function getFloatPrice()
    {
        return $this->floatPrice;
    }

    /**
     * @param float $floatPrice
     */
    public function setFloatPrice($floatPrice)
    {
        $this->floatPrice = $floatPrice;
    }

    /**
     * @return int
     */
    public function getIntStock()
    {
        return $this->intStock;
    }

    /**
     * @param int $intStock
     */
    public function setIntStock($intStock)
    {
        $this->intStock = $intStock;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PreFlush()
     */
    public function prePersistPreUpdatepreFlush()
    {
        $this->dtmTimestamp = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set strProductName
     *
     * @param string $strProductName
     *
     * @return ProductData
     */
    public function setStrProductName($strProductName)
    {
        $this->strProductName = $strProductName;

        return $this;
    }

    /**
     * Get strProductName
     *
     * @return string
     */
    public function getStrProductName()
    {
        return $this->strProductName;
    }

    /**
     * Set strproductDesc
     *
     * @param string $strProductDesc
     *
     * @return ProductData
     */
    public function setStrProductDesc($strProductDesc)
    {
        $this->strProductDesc = $strProductDesc;

        return $this;
    }

    /**
     * Get strProductDesc
     *
     * @return string
     */
    public function getStrProductDesc()
    {
        return $this->strProductDesc;
    }

    /**
     * Set strProductCode
     *
     * @param string $strProductCode
     *
     * @return ProductData
     */
    public function setStrProductCode($strProductCode)
    {
        $this->strProductCode = $strProductCode;

        return $this;
    }

    /**
     * Get strProductCode
     *
     * @return string
     */
    public function getStrProductCode()
    {
        return $this->strProductCode;
    }

    /**
     * Set dtmAdded
     *
     * @param \DateTime $dtmAdded
     *
     * @return ProductData
     */
    public function setDtmAdded($dtmAdded)
    {
        $this->dtmAdded = $dtmAdded;

        return $this;
    }

    /**
     * Get dtmAdded
     *
     * @return \DateTime
     */
    public function getDtmAdded()
    {
        return $this->dtmAdded;
    }

    /**
     * Set dtmDiscontinued
     *
     * @param \DateTime $dtmDiscontinued
     *
     * @return ProductData
     */
    public function setDtmDiscontinued($dtmDiscontinued)
    {
        $this->dtmDiscontinued = $dtmDiscontinued;

        return $this;
    }

    /**
     * Get dtmDiscontinued
     *
     * @return \DateTime
     */
    public function getDtmDiscontinued()
    {
        return $this->dtmDiscontinued;
    }
}
